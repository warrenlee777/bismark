//
//  AppDelegate.h
//  sample2
//
//  Created by hideo on 10/8/13.
//  Copyright (c) 2013 bismark. All rights reserved.
//

#import <UIKit/UIKit.h>

#include "bsmd.h"

@protocol AppDelegateDelegate<NSObject>

-(void)opened;
-(void)closeed;
-(void)started;
-(void)stopped;
-(void)seeked;
-(void)fileStart;
-(void)fileStop;
-(void)fileSeek;
-(void)channelMessage;
-(void)clock:(NSInteger)value;
-(void)tempo:(NSInteger)value;
-(void)signature:(NSInteger)value;

@end

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@property (weak) id<AppDelegateDelegate> delegate;

@property BSMD_FUNC *api;
@property BSMD_HANDLE handle;
@property NSInteger clocks;
@property NSInteger beatsPerMeasure;
@property NSInteger whatNoteIsTheBeat;
@property NSInteger clockTicksPerQuarterNote;
@property NSInteger ndNotesPerQuarterNote;
@property NSInteger ndNotesPerQuarterNoteDefault;

@property (nonatomic) NSString *loadSoundFont;

-(BOOL)checkIsSourceDeviceReady;
-(BOOL)checkIsDestinationDeviceReady;
- (void) listAllInterfaces:(void(^)(NSArray *sourceArray,NSArray *destionArray))success;
-(void)setSourceDevice:(NSInteger)index;
-(void)setDestionDevice:(NSInteger)index;
- (void) sendMidiDataInBackground;

@end
