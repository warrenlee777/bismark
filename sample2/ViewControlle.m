//
//  ViewController.m
//  sample
//
//  Created by hideo on 10/8/13.
//  Copyright (c) 2013 bismark. All rights reserved.
//

#import "AppDelegate.h"
#import "ViewControlle.h"
#import "TableViewController.h"
@import CoreAudioKit;

#define BYTE_TO_BINARY_PATTERN "%c%c%c%c%c%c%c%c"
#define BYTE_TO_BINARY(byte)  \
(byte & 0x80 ? '1' : '0'), \
(byte & 0x40 ? '1' : '0'), \
(byte & 0x20 ? '1' : '0'), \
(byte & 0x10 ? '1' : '0'), \
(byte & 0x08 ? '1' : '0'), \
(byte & 0x04 ? '1' : '0'), \
(byte & 0x02 ? '1' : '0'), \
(byte & 0x01 ? '1' : '0')

@interface ViewControlle ()<AppDelegateDelegate,UITableViewDataSource,UITableViewDelegate,TableViewControllerDele>
{
    NSTimer *timer;
    unsigned short division;
}

@property (weak, nonatomic) IBOutlet UISlider *seekSlider;
@property (weak, nonatomic) IBOutlet UISwitch *reverbSwitch;
@property (weak, nonatomic) IBOutlet UILabel *reverbs;
@property (weak, nonatomic) IBOutlet UILabel *reverbAvilable;
@property (weak, nonatomic) IBOutlet UISwitch *chorusSwitch;
@property (weak, nonatomic) IBOutlet UILabel *choru;
@property (weak, nonatomic) IBOutlet UILabel *chorusAviable;
@property (weak, nonatomic) IBOutlet UILabel *keyLabel;
@property (weak, nonatomic) IBOutlet UILabel *speedLabel;
@property (weak, nonatomic) IBOutlet UILabel *volumeLabel;
@property (weak, nonatomic) IBOutlet UILabel *tuneLabel;
@property (weak, nonatomic) IBOutlet UILabel *guideLabel;
@property (weak, nonatomic) IBOutlet UILabel *delayLabel;
@property (weak, nonatomic) IBOutlet UILabel *delayAviable;
@property (weak, nonatomic) IBOutlet UILabel *sampleRateLabel;
@property (weak, nonatomic) IBOutlet UILabel *channelsLabel;
@property (weak, nonatomic) IBOutlet UILabel *polyLabel;
@property (weak, nonatomic) IBOutlet UIScrollView *channelScrollView;
@property (weak, nonatomic) IBOutlet UITableView *midiTableView;
@property (weak, nonatomic) IBOutlet UITableView *soundFontTableView;
@property (nonatomic) NSMutableArray *trackVolumeArray;
@property (nonatomic) NSMutableArray *trackMuteArray;
@property (nonatomic) NSMutableArray *trackSoloArray;
@property (nonatomic) NSMutableArray *trackInstrumentArray;
@property (nonatomic) NSMutableArray *trackPanArray;
@property (nonatomic) NSMutableArray *instrumentArray;
@property (weak, nonatomic) IBOutlet UIStepper *keyStepper;
@property (weak, nonatomic) IBOutlet UIStepper *speedSteeper;
@property (weak, nonatomic) IBOutlet UIStepper *volumeSteeper;
@property (weak, nonatomic) IBOutlet UIStepper *tuneSteeper;
@property (weak, nonatomic) IBOutlet UIStepper *guideSteeper;
@property (weak, nonatomic) IBOutlet UIStepper *sampleSteeper;
@property (weak, nonatomic) IBOutlet UIStepper *delaySteeper;
@property (weak, nonatomic) IBOutlet UIStepper *polySteeper;
@property (weak, nonatomic) IBOutlet UILabel *soundNumber;
@property (weak, nonatomic) IBOutlet UILabel *soundMode;
@property (weak, nonatomic) IBOutlet UILabel *instrumentFix;
@property (nonatomic) NSArray *midiArray;
@property (nonatomic) NSMutableArray *soundFontArray;
@property (nonatomic) NSInteger currentMidiIndex;
@property (nonatomic) NSInteger currentSoundFontIndex;
@property (nonatomic) CGFloat currentSeekValue;
@property (weak, nonatomic) IBOutlet UILabel *barLabel;
@property (weak, nonatomic) IBOutlet UILabel *tempoLabel;
@property (weak, nonatomic) IBOutlet UILabel *singatureLabel;
@property (weak, nonatomic) IBOutlet UILabel *speedPercentLabel;


@end

@implementation ViewControlle


- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
    
    self.trackVolumeArray = [[NSMutableArray alloc] init];
    self.trackMuteArray = [[NSMutableArray alloc] init];
    self.trackSoloArray = [[NSMutableArray alloc] init];
    self.trackInstrumentArray = [[NSMutableArray alloc] init];
    self.trackPanArray = [[NSMutableArray alloc] init];
    self.soundFontArray = [[NSMutableArray alloc] init];
    self.midiArray = [[NSBundle mainBundle] URLsForResourcesWithExtension:@"midi" subdirectory:nil];
    NSArray *array = [[NSBundle mainBundle] URLsForResourcesWithExtension:@"dls" subdirectory:nil];
    for(NSString *string in array){
        [self.soundFontArray addObject:string];
    }
    array = [[NSBundle mainBundle] URLsForResourcesWithExtension:@"sf2" subdirectory:nil];
    for(NSString *string in array){
        [self.soundFontArray addObject:string];
    }
    
    UILabel *descriptionLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, self.channelScrollView.frame.size.width, 30)];
    [self.channelScrollView addSubview:descriptionLabel];
    descriptionLabel.text = @"instrument                           solo      mute     pan                volume";
    self.channelScrollView.contentSize = CGSizeMake(self.channelScrollView.frame.size.width, descriptionLabel.frame.size.height);
    for(int i=0;i<16;i++){
        [self addTrack];
    }
    
    AppDelegate *appDelegate = (AppDelegate *) [[UIApplication sharedApplication]delegate];
    appDelegate.delegate = self;
    
    self.midiTableView.dataSource = self;
    self.midiTableView.delegate = self;
    self.soundFontTableView.dataSource = self;
    self.soundFontTableView.delegate = self;
    
    self.currentMidiIndex = 0;
    self.currentSoundFontIndex = 0;
    
    [self.seekSlider addTarget:self action:@selector(seekSliderController) forControlEvents:UIControlEventTouchUpInside];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    
    [self loadMidi:[[((NSString*)[self.midiArray objectAtIndex:self.currentMidiIndex])lastPathComponent]stringByDeletingPathExtension]];
    
    
    
}

-(void)loadMidi:(NSString*)midiFilePath{
    AppDelegate *appDelegate = (AppDelegate *) [[UIApplication sharedApplication]delegate];
    BSMD_ERR err = BSMD_OK;
    
    if (err == BSMD_OK) {
        // revweb on
        int value = (int) self.reverbSwitch.isOn;
        err = appDelegate.api->ctrl (appDelegate.handle, BSMD_CTRL_SET_REVERB, &value, sizeof (value));
    }
    
    if (err == BSMD_OK) {
        // chorus on
        int value = (int) self.chorusSwitch.isOn;
        err = appDelegate.api->ctrl (appDelegate.handle, BSMD_CTRL_SET_CHORUS, &value, sizeof (value));
    }
    
    if (err == BSMD_OK) {
        // set midi contents
        NSString *path = [[NSBundle mainBundle] pathForResource:midiFilePath ofType:@"midi"];
        const char *lib = [path cStringUsingEncoding:NSShiftJISStringEncoding];
        err = appDelegate.api->setFile (appDelegate.handle, lib);
        
        
    }
    
    if (err == BSMD_OK) {
        [self initSeek];
        
        self.instrumentArray = [[NSMutableArray alloc] init];
        for(int i=0;i<16;i++){
            [self.instrumentArray addObject:[self getBSMP_CTRL_GET_INSTRUMENT_NAME:i]];
        }
        
        
        
    }
    
    if (err == BSMD_OK) {
        
        // open wave output device
        int numDrivers = appDelegate.api->getNumDrivers(appDelegate.handle);
        NSLog(@"numDrivers %d",numDrivers);
        LPCTSTR driverName = appDelegate.api->getDriverName(appDelegate.handle,0);
        NSLog(@"driverName %s",driverName);
        int numDevices = appDelegate.api->getNumDevice(appDelegate.handle,driverName);
        NSLog(@"numDevices %d",numDevices);
        LPCTSTR deviceName = appDelegate.api->getDeviceName(appDelegate.handle,driverName,0);
        NSLog(@"deviceName %s",deviceName);
        err = appDelegate.api->open (appDelegate.handle, NULL, NULL);
        if (err == BSMD_OK) {
           
            if (appDelegate.api->isPlaying (appDelegate.handle) == 0) {
                appDelegate.api->start (appDelegate.handle);
                timer = [NSTimer scheduledTimerWithTimeInterval:1.0f target:self selector:@selector(updateSlider:) userInfo:nil repeats:YES];
            }
            
        }
        
        
        [self getVolumeValue];
        [self getSpeedValue];
        [self getKeyValue];
        [self getTuneValue];
        [self getGuideValue];
        [self getBSMP_CTRL_GET_REVERBValue];
        [self getBSMP_CTRL_GET_REVERB_AVAILABLEValue];
        [self getBSMP_CTRL_GET_CHORUSValue];
        [self getBSMP_CTRL_GET_CHORUS_AVAILABLEValue];
        [self setBSMP_CTRL_GET_DELAY_AVAILABLEValue];
        [self getDelayValue];
        [self getBSMP_CTRL_GET_SOUND_LIBRARY_NUMValue];
        [self getBSMP_CTRL_GET_SOUND_LIBRARY_SELValue];
        [self getBSMP_CTRL_GET_NO_INSTRUMENT_FIXValue];
        [self getSampleRateValue];
        [self getChannelsValue];
        [self getPolyValue];
        
        
    }
}

- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    
    AppDelegate *appDelegate = (AppDelegate *) [[UIApplication sharedApplication]delegate];
    BSMD_ERR err = BSMD_OK;
    
    if (err == BSMD_OK) {
        // close wave output device
        err = appDelegate.api->close (appDelegate.handle);
    }
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}
- (IBAction)playMidi:(id)sender {
    AppDelegate *appDelegate = (AppDelegate *) [[UIApplication sharedApplication]delegate];
    [appDelegate sendMidiDataInBackground];
}
- (IBAction)externaDevice:(id)sender {
    AppDelegate *appDelegate = (AppDelegate *) [[UIApplication sharedApplication]delegate];
    if([appDelegate checkIsDestinationDeviceReady] == NO){
        dispatch_async(dispatch_get_main_queue(), ^(void){
            CABTMIDICentralViewController *viewController = [[CABTMIDICentralViewController alloc] init];
            
            [self.navigationController pushViewController:viewController animated:NO];
        });
    }else{
        UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Mai" bundle:nil];
        TableViewController *viewController = [storyboard instantiateViewControllerWithIdentifier:@"TableViewController"];
        viewController.dele = self;
        [self.navigationController pushViewController:viewController animated:NO];
    }
}

- (IBAction)start:(id)sender
{
    AppDelegate *appDelegate = (AppDelegate *) [[UIApplication sharedApplication]delegate];
    if (appDelegate.api->isFilePlaying (appDelegate.handle) == 0) {
        BSMD_ERR err = BSMD_OK;
        err = appDelegate.api->startFilePlay (appDelegate.handle);
        timer = [NSTimer scheduledTimerWithTimeInterval:1.0f target:self selector:@selector(updateSlider:) userInfo:nil repeats:YES];
    }
    [self trackVolumeSlider:nil];
}

- (IBAction)stop:(id)sender
{
    [self stopFile];
}

-(void)stopFile{
    AppDelegate *appDelegate = (AppDelegate *) [[UIApplication sharedApplication]delegate];
    if (appDelegate.api->isFilePlaying (appDelegate.handle) == 1) {
        appDelegate.api->stopFilePlay (appDelegate.handle);
        [timer invalidate];
    }
    
    
}

-(void)close{
    AppDelegate *appDelegate = (AppDelegate *) [[UIApplication sharedApplication]delegate];
    appDelegate.api->close(appDelegate.handle);
}

-(void)exit{
    AppDelegate *appDelegate = (AppDelegate *) [[UIApplication sharedApplication]delegate];
    appDelegate.api->exit(appDelegate.handle);
}

- (IBAction)seek:(UISlider *)sender
{
    NSLog(@"%f",sender.value);
    self.currentSeekValue = sender.value;
    
    
}

-(void)seekSliderController{
    AppDelegate *appDelegate = (AppDelegate *) [[UIApplication sharedApplication]delegate];
    
    unsigned long tick = self.currentSeekValue * division / 24;
    NSLog (@"seek  tick %lu value %f", tick, self.currentSeekValue);
    appDelegate.api->seekFilePlay (appDelegate.handle, tick);
}

- (void)updateSlider:(NSTimer *)timer
{
    AppDelegate *appDelegate = (AppDelegate *) [[UIApplication sharedApplication]delegate];
    if (self.seekSlider.touchInside == NO) {
        [self.seekSlider setValue:appDelegate.clocks];
    }
}

-(void)initSeek{
    AppDelegate *appDelegate = (AppDelegate *) [[UIApplication sharedApplication]delegate];
    unsigned long totaltime;
    unsigned long totaltick;
    appDelegate.api->getFileInfo (appDelegate.handle, NULL, &division, &totaltick, &totaltime);
    NSInteger clocks = totaltick * 24 / division;
    NSLog (@"total tick %lu clocks %ld totaltime %lu", totaltick, (long)clocks,totaltime);
    [self.seekSlider setMaximumValue:clocks];
    appDelegate.clocks = 0;
    [self.seekSlider setValue:appDelegate.clocks];
    unsigned long tick = 0.0 * division / 24;
    NSLog (@"seek %lu tick = %.0f MIDI clocks", tick, 0.0);
    BSMD_ERR err = BSMD_OK;
    err = appDelegate.api->seekFilePlay (appDelegate.handle, tick);
    if(err == BSMD_OK){
        
    }else{
        NSLog(@"");
    }
}

- (IBAction)keyControl:(UIStepper *)sender
{
    int value = sender.value;
    AppDelegate *appDelegate = (AppDelegate *) [[UIApplication sharedApplication]delegate];
    appDelegate.api->ctrl (appDelegate.handle, BSMD_CTRL_SET_MASTER_KEY, &value, sizeof (value));
    
    [self getKeyValue];
}

-(void)getKeyValue{
    int result;
    AppDelegate *appDelegate = (AppDelegate *) [[UIApplication sharedApplication]delegate];
    appDelegate.api->ctrl (appDelegate.handle, BSMD_CTRL_GET_MASTER_KEY, &result, sizeof (result));
    
    self.keyLabel.text = [NSString stringWithFormat:@"MASTER_KEY %D", result];
    
    self.keyStepper.value = result;
}

- (IBAction)speedControl:(UIStepper *)sender {
    int value = sender.value;
    AppDelegate *appDelegate = (AppDelegate *) [[UIApplication sharedApplication]delegate];
    appDelegate.api->ctrl (appDelegate.handle, BSMD_CTRL_SET_SPEED, &value, sizeof (value));
    
    [self getSpeedValue];
}

-(void)getSpeedValue{
    int result;
    AppDelegate *appDelegate = (AppDelegate *) [[UIApplication sharedApplication]delegate];
    appDelegate.api->ctrl (appDelegate.handle, BSMD_CTRL_GET_SPEED, &result, sizeof (result));
    
    self.speedLabel.text = [NSString stringWithFormat:@"SPEED %d",result];
    
    self.speedSteeper.value = result;
}

- (IBAction)reverb:(UISwitch *)sender
{
    int value = sender.isOn;
    AppDelegate *appDelegate = (AppDelegate *) [[UIApplication sharedApplication]delegate];
    appDelegate.api->ctrl (appDelegate.handle, BSMD_CTRL_SET_REVERB, &value, sizeof (value));
    
    [self getBSMP_CTRL_GET_REVERBValue];
}

-(void)getBSMP_CTRL_GET_REVERBValue{
    int result;
    AppDelegate *appDelegate = (AppDelegate *) [[UIApplication sharedApplication]delegate];
    appDelegate.api->ctrl (appDelegate.handle, BSMD_CTRL_GET_REVERB, &result, sizeof (result));
    
    self.reverbs.text = [NSString stringWithFormat:@"REVERB %d",result];
}

-(void)getBSMP_CTRL_GET_REVERB_AVAILABLEValue{
    int result;
    AppDelegate *appDelegate = (AppDelegate *) [[UIApplication sharedApplication]delegate];
    appDelegate.api->ctrl (appDelegate.handle, BSMD_CTRL_GET_REVERB_AVAILABLE, &result, sizeof (result));
    
    self.reverbAvilable.text = [NSString stringWithFormat:@"AVAILABLE %d",result];
}

- (IBAction)chorus:(UISwitch *)sender
{
    int value = sender.isOn;
    AppDelegate *appDelegate = (AppDelegate *) [[UIApplication sharedApplication]delegate];
    appDelegate.api->ctrl (appDelegate.handle, BSMD_CTRL_SET_CHORUS, &value, sizeof (value));
    
    [self getBSMP_CTRL_GET_CHORUSValue];
}

-(void)getBSMP_CTRL_GET_CHORUSValue{
    int result;
    AppDelegate *appDelegate = (AppDelegate *) [[UIApplication sharedApplication]delegate];
    appDelegate.api->ctrl (appDelegate.handle, BSMD_CTRL_GET_CHORUS, &result, sizeof (result));
    
    self.choru.text = [NSString stringWithFormat:@"CHORUS %d",result];
}

-(void)getBSMP_CTRL_GET_CHORUS_AVAILABLEValue{
    int result;
    AppDelegate *appDelegate = (AppDelegate *) [[UIApplication sharedApplication]delegate];
    appDelegate.api->ctrl (appDelegate.handle, BSMD_CTRL_GET_CHORUS_AVAILABLE, &result, sizeof (result));
    
    self.chorusAviable.text = [NSString stringWithFormat:@"AVAILABLE %d",result];
}

- (IBAction)volume:(UIStepper *)sender {
    int value = sender.value;
    AppDelegate *appDelegate = (AppDelegate *) [[UIApplication sharedApplication]delegate];
    BSMD_ERR err = BSMD_OK;
    err = appDelegate.api->ctrl (appDelegate.handle, BSMD_CTRL_SET_MASTER_VOLUME, &value, sizeof (value));
    if (err == BSMD_OK) {


    }else{
        NSLog(@"%d",err);
    }
    [self getVolumeValue];
    
    
}

-(void)setAllVolume:(int)volume{
    for(int i=0;i<[self.trackVolumeArray count];i++){
        [self setVolume:volume withTrack:i];
        
    }
}

-(void)getVolumeValue{
    int result;
    AppDelegate *appDelegate = (AppDelegate *) [[UIApplication sharedApplication]delegate];
    appDelegate.api->ctrl (appDelegate.handle, BSMD_CTRL_GET_MASTER_VOLUME, &result, sizeof (result));
    
    self.volumeLabel.text = [NSString stringWithFormat:@"MASTER_VOLUME %d",result];
    self.volumeSteeper.value = result;
}
- (IBAction)volumeSlider:(UISlider*)sender {
    int value = sender.value;
    NSLog(@"%d",value);
    [self setAllVolume:value];
}

- (IBAction)tune:(UIStepper*)sender {
    int value = sender.value;
    AppDelegate *appDelegate = (AppDelegate *) [[UIApplication sharedApplication]delegate];
    appDelegate.api->ctrl (appDelegate.handle, BSMD_CTRL_SET_MASTER_TUNE, &value, sizeof (value));
    
    [self getTuneValue];
}

-(void)getTuneValue{
    int result;
    AppDelegate *appDelegate = (AppDelegate *) [[UIApplication sharedApplication]delegate];
    appDelegate.api->ctrl (appDelegate.handle, BSMD_CTRL_GET_MASTER_TUNE, &result, sizeof (result));
    
    self.tuneLabel.text = [NSString stringWithFormat:@"MASTER_TUNE %d",result];
    self.tuneSteeper.value = result;
}

- (IBAction)guide:(UIStepper*)sender {
   
}

-(void)getGuideValue{
    
}

- (IBAction)delay:(UIStepper*)sender {
    
    int value = sender.value;
    AppDelegate *appDelegate = (AppDelegate *) [[UIApplication sharedApplication]delegate];
    appDelegate.api->ctrl (appDelegate.handle, BSMD_CTRL_SET_DELAY, &value, sizeof (value));
    
    [self getDelayValue];
    
}

-(void)getDelayValue{
    int result;
    AppDelegate *appDelegate = (AppDelegate *) [[UIApplication sharedApplication]delegate];
    appDelegate.api->ctrl (appDelegate.handle, BSMD_CTRL_GET_DELAY, &result, sizeof (result));
    
    self.delayLabel.text = [NSString stringWithFormat:@"DELAY %d",result];
    self.delaySteeper.value = result;
}

-(void)setBSMP_CTRL_GET_DELAY_AVAILABLEValue{
    int result;
    AppDelegate *appDelegate = (AppDelegate *) [[UIApplication sharedApplication]delegate];
    appDelegate.api->ctrl (appDelegate.handle, BSMD_CTRL_GET_DELAY_AVAILABLE, &result, sizeof (result));
    
    self.delayAviable.text = [NSString stringWithFormat:@"AVAILABLE %d",result];
}

- (IBAction)sampleRate:(UIStepper*)sender {
    int value = sender.value;
    AppDelegate *appDelegate = (AppDelegate *) [[UIApplication sharedApplication]delegate];
    appDelegate.api->ctrl (appDelegate.handle, BSMD_CTRL_SET_SAMPLE_RATE, &value, sizeof (value));
    
    [self getSampleRateValue];
}

-(void)getSampleRateValue{
    int result;
    AppDelegate *appDelegate = (AppDelegate *) [[UIApplication sharedApplication]delegate];
    appDelegate.api->ctrl (appDelegate.handle, BSMD_CTRL_GET_SAMPLE_RATE, &result, sizeof (result));
    
    self.sampleRateLabel.text = [NSString stringWithFormat:@"SAMPLE_RATE %d",result];
    self.sampleSteeper.value = result;
}

-(void)getChannelsValue{
    int result;
    AppDelegate *appDelegate = (AppDelegate *) [[UIApplication sharedApplication]delegate];
    appDelegate.api->ctrl (appDelegate.handle, BSMD_CTRL_GET_CHANNELS, &result, sizeof (result));
    
    self.channelsLabel.text = [NSString stringWithFormat:@"CHANNELS %d",result];
}

- (IBAction)poly:(UIStepper*)sender {
    int value = sender.value;
    AppDelegate *appDelegate = (AppDelegate *) [[UIApplication sharedApplication]delegate];
    appDelegate.api->ctrl (appDelegate.handle, BSMD_CTRL_SET_POLY, &value, sizeof (value));
    
    [self getPolyValue];
}

-(void)getPolyValue{
    int result;
    AppDelegate *appDelegate = (AppDelegate *) [[UIApplication sharedApplication]delegate];
    appDelegate.api->ctrl (appDelegate.handle, BSMD_CTRL_GET_POLY, &result, sizeof (result));
    
    self.polyLabel.text = [NSString stringWithFormat:@"POLY %d",result];
}

- (IBAction)INSTRUMENT_FIX:(UISwitch*)sender {
    int value = sender.isOn;
    AppDelegate *appDelegate = (AppDelegate *) [[UIApplication sharedApplication]delegate];
    appDelegate.api->ctrl (appDelegate.handle, BSMD_CTRL_SET_NO_INSTRUMENT_FIX, &value, sizeof (value));
}

-(void)getBSMP_CTRL_GET_NO_INSTRUMENT_FIXValue{
    int result;
    AppDelegate *appDelegate = (AppDelegate *) [[UIApplication sharedApplication]delegate];
    appDelegate.api->ctrl (appDelegate.handle, BSMD_CTRL_GET_NO_INSTRUMENT_FIX, &result, sizeof (result));
    
    self.instrumentFix.text = [NSString stringWithFormat:@"INSTRUMENT_FIX %d",result];
}

-(NSString*)getBSMP_CTRL_GET_INSTRUMENT_NAME:(int)track{
    char result[100];
    AppDelegate *appDelegate = (AppDelegate *) [[UIApplication sharedApplication]delegate];
    BSMD_ERR err = BSMD_OK;
    err = appDelegate.api->ctrl (appDelegate.handle, BSMD_CTRL_GET_INSTRUMENT_NAME+track , result, sizeof (result));
    if(err == BSMD_OK){
        NSLog(@"ok");
    }else{
        NSLog(@"%d",err);
    }
    //    NSLog(@"instrument name %@",[NSString stringWithFormat:@"%s", result]);
    return [NSString stringWithFormat:@"%s", result];
}

-(void)getBSMP_CTRL_GET_SOUND_LIBRARY_NUMValue{
    int result;
    AppDelegate *appDelegate = (AppDelegate *) [[UIApplication sharedApplication]delegate];
    appDelegate.api->ctrl (appDelegate.handle, BSMD_CTRL_GET_SOUND_LIBRARY_NUM, &result, sizeof (result));
    
    self.soundNumber.text = [NSString stringWithFormat:@"SOUND_LIBRARY %d",result];
    
}

-(void)getBSMP_CTRL_GET_SOUND_LIBRARY_SELValue{
    int result;
    AppDelegate *appDelegate = (AppDelegate *) [[UIApplication sharedApplication]delegate];
    appDelegate.api->ctrl (appDelegate.handle, BSMD_CTRL_GET_SOUND_LIBRARY_SEL, &result, sizeof (result));
    
    self.soundMode.text = [NSString stringWithFormat:@"SOUND_MODE %d",result];
}

-(void)addTrack{
    UISlider *sliderVolume = [[UISlider alloc] initWithFrame:CGRectMake(420, self.channelScrollView.contentSize.height, self.channelScrollView.frame.size.width-420, 30)];
    [self.channelScrollView addSubview:sliderVolume];
    sliderVolume.minimumValue = 0;
    sliderVolume.maximumValue = 127;
    sliderVolume.value = 63;
    [sliderVolume addTarget:self action:@selector(trackVolumeSlider:) forControlEvents:UIControlEventValueChanged];
    UISlider *sliderPan = [[UISlider alloc] initWithFrame:CGRectMake(320, self.channelScrollView.contentSize.height, 100, 30)];
    [self.channelScrollView addSubview:sliderPan];
    sliderPan.minimumValue = 0;
    sliderPan.maximumValue = 127;
    sliderPan.value = 63;
    [sliderPan addTarget:self action:@selector(trackPanSlider:) forControlEvents:UIControlEventValueChanged];
    UISwitch *mute = [[UISwitch alloc] initWithFrame:CGRectMake(260, sliderVolume.frame.origin.y, 60, 30)];
    [mute addTarget:self action:@selector(muteSwitch:) forControlEvents:UIControlEventValueChanged];
    [self.channelScrollView addSubview:mute];
    UISwitch *solo = [[UISwitch alloc] initWithFrame:CGRectMake(200, sliderVolume.frame.origin.y, 60, 30)];
    [solo addTarget:self action:@selector(soloSwitch:) forControlEvents:UIControlEventValueChanged];
    [self.channelScrollView addSubview:solo];
    UILabel *instrument = [[UILabel alloc] initWithFrame:CGRectMake(0, sliderVolume.frame.origin.y, 200, 30)];
    [self.channelScrollView addSubview:instrument];
    [self.trackVolumeArray addObject:sliderVolume];
    [self.trackPanArray addObject:sliderPan];
    [self.trackMuteArray addObject:mute];
    [self.trackSoloArray addObject:solo];
    [self.trackInstrumentArray addObject:instrument];
    self.channelScrollView.contentSize = CGSizeMake(self.channelScrollView.frame.size.width, sliderVolume.frame.origin.y+sliderVolume.frame.size.height);
}

-(void)removeAllTrack{
    for(UIView *view in self.trackVolumeArray){
        [view removeFromSuperview];
    }
    [self.trackVolumeArray removeAllObjects];
    for(UIView *view in self.trackPanArray){
        [view removeFromSuperview];
    }
    [self.trackPanArray removeAllObjects];
    for(UIView *view in self.trackMuteArray){
        [view removeFromSuperview];
    }
    [self.trackMuteArray removeAllObjects];
    for(UIView *view in self.trackSoloArray){
        [view removeFromSuperview];
    }
    [self.trackSoloArray removeAllObjects];
    for(UIView *view in self.trackInstrumentArray){
        [view removeFromSuperview];
    }
    [self.trackInstrumentArray removeAllObjects];
}

-(void)trackVolumeSlider:(UISlider*)sender{
    for(int i=0;i<[self.trackVolumeArray count];i++){
        if(sender == [self.trackVolumeArray objectAtIndex:i]){
            [self setVolume:sender.value withTrack:i];
            break;
        }
        
    }
}

-(void)setVolume:(int)volume withTrack:(int)track{
    AppDelegate *appDelegate = (AppDelegate *) [[UIApplication sharedApplication]delegate];
    
    unsigned char port = 0;
    unsigned char status = 0xB0;
    unsigned char channel = track;
    unsigned char data1 = 0x07;
    unsigned char data2 = volume;
    printf("setChannelMessage port "BYTE_TO_BINARY_PATTERN" \n", BYTE_TO_BINARY(port));
    printf("setChannelMessage status "BYTE_TO_BINARY_PATTERN" \n", BYTE_TO_BINARY(status));
    printf("setChannelMessage channel "BYTE_TO_BINARY_PATTERN" \n", BYTE_TO_BINARY(channel));
    printf("setChannelMessage data1 "BYTE_TO_BINARY_PATTERN" \n", BYTE_TO_BINARY(data1));
    printf("setChannelMessage data2 "BYTE_TO_BINARY_PATTERN" \n", BYTE_TO_BINARY(data2));
    printf("\n");
    appDelegate.api->setChannelMessage(appDelegate.handle,0,status+channel,data1,data2);
}

-(void)trackPanSlider:(UISlider*)sender{
    for(int i=0;i<[self.trackPanArray count];i++){
        if(sender == [self.trackPanArray objectAtIndex:i]){
            AppDelegate *appDelegate = (AppDelegate *) [[UIApplication sharedApplication]delegate];
            
            unsigned char port = 0;
            unsigned char status = 0xB0;
            unsigned char channel = i;
            unsigned char data1 = 0x0A;
            unsigned char data2 = sender.value;
            printf("setChannelMessage port "BYTE_TO_BINARY_PATTERN" \n", BYTE_TO_BINARY(port));
            printf("setChannelMessage status "BYTE_TO_BINARY_PATTERN" \n", BYTE_TO_BINARY(status));
            printf("setChannelMessage channel "BYTE_TO_BINARY_PATTERN" \n", BYTE_TO_BINARY(channel));
            printf("setChannelMessage data1 "BYTE_TO_BINARY_PATTERN" \n", BYTE_TO_BINARY(data1));
            printf("setChannelMessage data2 "BYTE_TO_BINARY_PATTERN" \n", BYTE_TO_BINARY(data2));
            printf("\n");
            appDelegate.api->setChannelMessage(appDelegate.handle,0,status+channel,data1,data2);
            break;
        }
        
    }
}



-(void)setBPMDefault{
    AppDelegate *appDelegate = (AppDelegate *) [[UIApplication sharedApplication]delegate];
    
    
    int value = 0;
    appDelegate.api->ctrl (appDelegate.handle, BSMD_CTRL_SET_SPEED, &value, sizeof (value));
}
-(void)muteSwitch:(UISwitch*)sender{
    int track = 0;
    int value;
    if(sender.isOn){
        value = 1;
    }else{
        value = 0;
    }
    for(int i=0;i<[self.trackMuteArray count];i++){
        UISwitch *uiSwitch = [self.trackMuteArray objectAtIndex:i];
        if(uiSwitch == sender){
            track = i;
        }
    }
    
    
    AppDelegate *appDelegate = (AppDelegate *) [[UIApplication sharedApplication]delegate];
    appDelegate.api->ctrl (appDelegate.handle, BSMD_CTRL_SET_MUTE+track, &value, sizeof (value));
}

-(void)soloSwitch:(UISwitch*)sender{
    int track = 0;
    int value;
    if(sender.isOn){
        value = 1;
    }else{
        value = 0;
    }
    for(int i=0;i<[self.trackSoloArray count];i++){
        UISwitch *uiSwitch = [self.trackSoloArray objectAtIndex:i];
        if(uiSwitch == sender){
            track = i;
        }else{
            if(sender.isOn){
                [uiSwitch setOn:NO];
            }
        }
        
    }
    
    
    AppDelegate *appDelegate = (AppDelegate *) [[UIApplication sharedApplication]delegate];
    appDelegate.api->ctrl (appDelegate.handle, BSMD_CTRL_SET_SOLO+track, &value, sizeof (value));
}

-(void)updateTrackInstrument:(int)track withName:(NSString*)name{
    UILabel *label = [self.trackInstrumentArray objectAtIndex:track];
    label.text = name;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    if(tableView == self.midiTableView){
        return [self.midiArray count];
    }else if(tableView == self.soundFontTableView){
        return [self.soundFontArray count];
    }
    return 0;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    UITableViewCell *cell = [[UITableViewCell alloc] init];
    if(tableView == self.midiTableView){
        cell.textLabel.text = [[[self.midiArray objectAtIndex:indexPath.row] lastPathComponent] stringByDeletingPathExtension];
    }else if(tableView == self.soundFontTableView){
        cell.textLabel.text = [[[self.soundFontArray objectAtIndex:indexPath.row] lastPathComponent] stringByDeletingPathExtension];
    }
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    if(tableView == self.midiTableView){
        if(self.currentMidiIndex != indexPath.row){
            self.currentMidiIndex = indexPath.row;
            [self stopFile];
            [self loadMidi:[[((NSString*)[self.midiArray objectAtIndex:self.currentMidiIndex])lastPathComponent]stringByDeletingPathExtension]];
        }
    }else if(tableView == self.soundFontTableView){
        if(self.currentSoundFontIndex != indexPath.row){
            self.currentSoundFontIndex = indexPath.row;
            [self stopFile];
            [self close];
            [self exit];
            AppDelegate *appDelegate = (AppDelegate *) [[UIApplication sharedApplication]delegate];
            appDelegate.loadSoundFont = [self.soundFontArray objectAtIndex:self.currentSoundFontIndex];
            [self loadMidi:[[((NSString*)[self.midiArray objectAtIndex:self.currentMidiIndex])lastPathComponent]stringByDeletingPathExtension]];
        }
    }
    
    
}

-(void)opened{
    
    //    for(int i=0;i<16;i++){
    //        [self updateTrackInstrument:i];
    //    }
}

-(void)closeed{
    
    //    for(int i=0;i<16;i++){
    //        [self updateTrackInstrument:i];
    //    }
}

-(void)started{
    
    //    for(int i=0;i<16;i++){
    //        [self updateTrackInstrument:i];
    //    }
}

-(void)stopped{
    
    //    for(int i=0;i<16;i++){
    //        [self updateTrackInstrument:i];
    //    }
}

-(void)fileStart{
    
}

-(void)fileStop{
    
}

-(void)fileSeek{
    [self setBPMDefault];
    for(int i=0;i<16;i++){
        dispatch_async(dispatch_get_main_queue(), ^(void){
            NSString *name = [self getBSMP_CTRL_GET_INSTRUMENT_NAME:i];
            [self updateTrackInstrument:i withName:name];
        });
        
    }
}

-(void)seeked{
    
    for(int i=0;i<16;i++){
        dispatch_async(dispatch_get_main_queue(), ^(void){
            NSString *name = [self getBSMP_CTRL_GET_INSTRUMENT_NAME:i];
            [self updateTrackInstrument:i withName:name];
        });
        
    }
}

-(void)channelMessage{
    for(int i=0;i<16;i++){
        dispatch_async(dispatch_get_main_queue(), ^(void){
            NSString *oldName = [self.instrumentArray objectAtIndex:i];
            NSString *name = [self getBSMP_CTRL_GET_INSTRUMENT_NAME:i];
            if(![oldName isEqualToString:name]){
                [self updateTrackInstrument:i withName:name];
            }
        });
        
        
    }
}

-(void)tempo:(NSInteger)value{
    dispatch_async(dispatch_get_main_queue(), ^(void){
        int bpm = 60000000/value;
        self.tempoLabel.text = [NSString stringWithFormat:@"tempo %d",bpm];
    });
    
}

-(void)clock:(NSInteger)value{
    dispatch_async(dispatch_get_main_queue(), ^(void){
        AppDelegate *appDelegate = (AppDelegate *) [[UIApplication sharedApplication]delegate];
        NSInteger beatsPerMeasure = appDelegate.beatsPerMeasure;
        NSInteger clockTicksPerQuarterNote = appDelegate.clockTicksPerQuarterNote;
        NSInteger clocks = appDelegate.clocks;
        NSInteger beat = (CGFloat)clocks/(CGFloat)clockTicksPerQuarterNote;
        NSInteger measure = (int)((CGFloat)beat/(CGFloat)beatsPerMeasure)+1;
        NSInteger beatInMeasure = (beat%beatsPerMeasure)+1;
        self.barLabel.text = [NSString stringWithFormat:@"bar %ld %ld",(long)measure,(long)beatInMeasure];
    });
}

-(void)signature:(NSInteger)value{
    dispatch_async(dispatch_get_main_queue(), ^(void){
         AppDelegate *appDelegate = (AppDelegate *) [[UIApplication sharedApplication]delegate];
        self.singatureLabel.text = [NSString stringWithFormat:@"%d/%d",(int)appDelegate.beatsPerMeasure,(int)pow(appDelegate.whatNoteIsTheBeat,2)];
        CGFloat percent = (CGFloat)appDelegate.ndNotesPerQuarterNote/(CGFloat)appDelegate.ndNotesPerQuarterNoteDefault;
        
        self.speedPercentLabel.text = [NSString stringWithFormat:@"speed %f",percent];
    });
}

-(void)selectSouece:(NSInteger)index{
    
}

-(void)selectDestion:(NSInteger)index{
    
}

@end

