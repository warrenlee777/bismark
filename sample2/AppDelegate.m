//
//  AppDelegate.m
//  sample2
//
//  Created by hideo on 10/8/13.
//  Copyright (c) 2013 bismark. All rights reserved.
//

@import AVFoundation;

#import "AppDelegate.h"
#import "PGMidi.h"

@interface AppDelegate()<PGMidiDelegate, PGMidiSourceDelegate>
- (void)callback_BSMD_CALLBACK:(BSMD_CALLBACK_TYPE) type data:(void *)data;
- (void)callback_BSMD_CALLBACK_BOUNCE:(int) percent;

@property (nonatomic,strong) PGMidi *midi;

@property (nonatomic) NSMutableArray *souceArray;
@property (nonatomic) NSMutableArray *destionArray;
@property (nonatomic) CGFloat currentTime;
@property (nonatomic) unsigned int currentTempo;
@property (nonatomic) unsigned int microsecondsPerQuarterNote;

@end

static void myCallback_BSMD_CALLBACK (BSMD_HANDLE handle, BSMD_CALLBACK_TYPE type, void *data, void *user)
{
    AppDelegate *appDelegate = (__bridge AppDelegate *) user;
    [appDelegate callback_BSMD_CALLBACK:type data:data];
}

@implementation AppDelegate

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions
{
  // Override point for customization after application launch.
    
    self.ndNotesPerQuarterNoteDefault = -1;
    self.currentTime = 0;
    self.clockTicksPerQuarterNote = 24;

  AVAudioSession *audioSession = [AVAudioSession sharedInstance];
  [audioSession setCategory:AVAudioSessionCategoryAmbient error:nil];
  [audioSession setActive:YES error:nil];
  [audioSession setPreferredIOBufferDuration:0.005 error:nil];

    [self loadSoundFont:nil];
    
    [self loadMidiDevice];
    
    [self listAllInterfaces:^(NSArray *sourceArray, NSArray *destionArray) {
        
    }];

  return YES;
}

-(void)loadSoundFont:(NSString *)pathString{
    self.api = bsmdLoad ();
    BSMD_ERR err = BSMD_OK;
    
    if (err == BSMD_OK) {
        // initialize
        NSString *path;
        if(pathString == nil){
            path = [[NSBundle mainBundle] pathForResource:@"GeneralUser GS SoftSynth v1.44" ofType:@"sf2"];
        }else{
            path = [[NSBundle mainBundle] pathForResource:[[pathString lastPathComponent]stringByDeletingPathExtension] ofType:[pathString pathExtension]];
        }
        
        const char *lib = [path cStringUsingEncoding:NSASCIIStringEncoding];
        
        // This keycode will be expire on 2018/03/31
        const unsigned char key[64] = {
            0xA9, 0x89, 0x5F, 0x31, 0xDF, 0x5E, 0x40, 0xE5,
            0x15, 0xDC, 0x93, 0x02, 0xDC, 0x97, 0xFC, 0x85,
            0xB5, 0x9F, 0x5B, 0x43, 0x22, 0x73, 0xE3, 0x34,
            0x92, 0x70, 0x70, 0xFA, 0x6B, 0xFC, 0xC6, 0x97,
            0x40, 0x21, 0xD0, 0xF4, 0x30, 0x91, 0x10, 0x54,
            0x56, 0x2F, 0xEE, 0x9B, 0xEF, 0x42, 0x50, 0x1C,
            0x9A, 0xB7, 0xC9, 0x1A, 0xEF, 0x7F, 0x1B, 0x76,
            0x33, 0x8E, 0x2A, 0xB2, 0x7E, 0x78, 0x0F, 0x69,
        };
        
        err = self.api->initializeWithSoundLib (&_handle, myCallback_BSMD_CALLBACK, (__bridge void *) self, (const char *) lib, NULL, key);
    }
    
    if (err == BSMD_OK) {
        int value = 512;
        self.api->ctrl (self.handle, BSMD_CTRL_SET_NUMBER_OF_REGIONS, &value, sizeof (value));
    }
    
    if (err != BSMD_OK) {
        NSLog (@"ERROR - initialize synthesizer");
    }
}

-(void)setLoadSoundFont:(NSString *)loadSoundFont{
    [self loadSoundFont:loadSoundFont];
}

- (void) listAllInterfaces:(void (^)(NSArray *, NSArray *))success{
    
    NSMutableArray *sourceNameArray = [[NSMutableArray alloc] init];
    NSMutableArray *destionNameArray = [[NSMutableArray alloc] init];
    self.souceArray = [[NSMutableArray alloc] init];
    self.destionArray = [[NSMutableArray alloc] init];
    for (PGMidiSource *source in self.midi.sources){
        NSLog(@"%@ %@",source,ToString(source));
        [sourceNameArray addObject:ToString(source)];
        [self.souceArray addObject:source];
    }
    
    for (PGMidiDestination *destination in self.midi.destinations){
        NSLog(@"%@ %@",destination,ToString(destination));
        [destionNameArray addObject:ToString(destination)];
        [self.destionArray addObject:destination];
    }
    
    success(sourceNameArray,destionNameArray);
}

NSString *ToString(PGMidiConnection *connection)
{
    return [NSString stringWithFormat:@"< PGMidiConnection: name=%@ isNetwork=%d >",
            connection.name, connection.isNetworkSession];
}

-(void)deattachToAllExistingSources {
    for (PGMidiSource *source in self.midi.sources){
        [source removeDelegate:self];
    }
}

- (void) attachToAllExistingSources{
    for (PGMidiSource *source in self.midi.sources){
        [source addDelegate:self];
    }
}


- (void) midi:(PGMidi*)midi sourceAdded:(PGMidiSource *)source{
    [source addDelegate:self];
}

- (void) midi:(PGMidi*)midi sourceRemoved:(PGMidiSource *)source{
    
}

- (void) midi:(PGMidi*)midi destinationAdded:(PGMidiDestination *)destination{
    
}

- (void) midi:(PGMidi*)midi destinationRemoved:(PGMidiDestination *)destination{
    
}

NSString *StringFromPacket(const MIDIPacket *packet){
    // Note - this is not an example of MIDI parsing. I'm just dumping
    // some bytes for diagnostics.
    // See comments in PGMidiSourceDelegate for an example of how to
    // interpret the MIDIPacket structure.
    NSMutableString *string = [[NSMutableString alloc] init];
    for(int i=0;i<packet->length;i++){
        [string appendFormat:@" %02x ",packet->data[i]];
        if (i%3 == 2) {
            [string appendFormat:@" | "];
        }
    }
    return [NSString stringWithFormat:@"  %u bytes: %@",
            packet->length,
            string
            ];
}

- (void) midiSource:(PGMidiSource*)midi midiReceived:(const MIDIPacketList *)packetList{
    
    
    NSMutableArray *pressArray = [[NSMutableArray alloc] init];
    NSMutableArray *releaseArray = [[NSMutableArray alloc] init];
    const MIDIPacket *packet = &packetList->packet[0];
    for (int i = 0; i < packetList->numPackets; ++i){
        for(int j=0;j<packet->length;j++){
            if (j %3 == 0) {
                if (packet->data[j] == 0x90) {
                    printf("down %d \n",packet->data[j+1]);
                    [pressArray addObject:[[NSNumber alloc] initWithInt:packet->data[j+1]]];
                }else if(packet->data[j] == 0x80){
                    printf("up %d \n",packet->data[j+1]);
                    [releaseArray addObject:[[NSNumber alloc] initWithInt:packet->data[j+1]]];
                }
            }
            
        }
        
        packet = MIDIPacketNext(packet);
    }
    
    
}

- (void) sendMidiDataInBackground
{
    for (int n = 0; n < 20; ++n)
    {
        const UInt8 note      = 1;
        const UInt8 noteOn[]  = { 0x90, note, 127 };
        const UInt8 noteOff[] = { 0x80, note, 0   };
        
        [self.midi sendBytes:noteOn size:sizeof(noteOn)];
        [NSThread sleepForTimeInterval:0.1];
        [self.midi sendBytes:noteOff size:sizeof(noteOff)];
    }
}

-(BOOL)checkIsSourceDeviceReady{
    if([self.midi.sources count] > 1){
        return YES;
    }
    return NO;
}

-(BOOL)checkIsDestinationDeviceReady{
    if([self.midi.destinations count] > 1){
        return YES;
    }
    return NO;
}

-(void)loadMidiDevice{
    self.midi = [[PGMidi alloc] init];
    self.midi.delegate = self;
    
    [self attachToAllExistingSources];
    
    self.midi.networkEnabled             = YES;
    self.midi.virtualDestinationEnabled  = YES;
    self.midi.virtualSourceEnabled       = YES;
}

-(void)removeMidiDevice{
    if (self.midi) {
        [self deattachToAllExistingSources];
        [self.midi releaseAll];
        self.midi.delegate = nil;
        self.midi = nil;
        
    }
}

-(void)setSourceDevice:(NSInteger)index{
    
}

-(void)setDestionDevice:(NSInteger)index{
    
}
							
- (void)applicationWillResignActive:(UIApplication *)application
{
  // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
  // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
}

- (void)applicationDidEnterBackground:(UIApplication *)application
{
  // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later. 
  // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
}

- (void)applicationWillEnterForeground:(UIApplication *)application
{
  // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
}

- (void)applicationDidBecomeActive:(UIApplication *)application
{
  // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
}

- (void)applicationWillTerminate:(UIApplication *)application
{
  // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
}

- (void)callback_BSMD_CALLBACK:(BSMD_CALLBACK_TYPE) type data:(void *)data
{
    switch (type) {
        case BSMD_CALLBACK_TYPE_NULL:
            break;
        case BSMD_CALLBACK_TYPE_OPEN:
            NSLog (@"opened");
            if([self.delegate respondsToSelector:@selector(opened)]){
                [self.delegate opened];
            }
            break;
        case BSMD_CALLBACK_TYPE_CLOSE:
            NSLog (@"closeed");
            if([self.delegate respondsToSelector:@selector(closeed)]){
                [self.delegate closeed];
            }
            break;
        case BSMD_CALLBACK_TYPE_START:
            NSLog (@"started");
            if([self.delegate respondsToSelector:@selector(started)]){
                [self.delegate started];
            }
            break;
        case BSMD_CALLBACK_TYPE_STOP:
            NSLog (@"stopped");
            if([self.delegate respondsToSelector:@selector(stopped)]){
                [self.delegate stopped];
            }
            break;
        case BSMD_CALLBACK_TYPE_FRAME:
//            NSLog (@"frame");
            
            break;
        case BSMD_CALLBACK_TYPE_FILE_START:
            //            printf("clocks %ld",(long)self.clocks);
            NSLog (@"FILE_START");
            if([self.delegate respondsToSelector:@selector(fileStart)]){
                [self.delegate fileStart];
            }
            break;
        case BSMD_CALLBACK_TYPE_FILE_STOP:
            //            printf("clocks %ld",(long)self.clocks);
            NSLog (@"FILE_STOP");
            if([self.delegate respondsToSelector:@selector(fileStop)]){
                [self.delegate fileStop];
            }
            break;
        case BSMD_CALLBACK_TYPE_FILE_SEEK:
            //            printf("clocks %ld",(long)self.clocks);
            self.clocks = 0;
            self.currentTime = 0;
            NSLog (@"FILE_SEEK");
            if([self.delegate respondsToSelector:@selector(fileSeek)]){
                [self.delegate fileSeek];
            }
            break;
        case BSMD_CALLBACK_TYPE_CLOCK:
            //            printf("clocks %ld",(long)self.clocks);
            self.clocks++;
            [self calculateCurrentTime];
            if([self.delegate respondsToSelector:@selector(clock:)]){
                [self.delegate clock:self.clocks];
            }
            break;
        case BSMD_CALLBACK_TYPE_TEMPO:
#if __LP64__
            NSLog (@"tempo = %u[usec/beat]", *(unsigned int *) data);
#else
            NSLog (@"tempo = %lu[usec/beat]", *(unsigned long *) data);
#endif
            self.microsecondsPerQuarterNote = *(unsigned int *) data;
            self.currentTempo = *(unsigned int *) data;
            if([self.delegate respondsToSelector:@selector(tempo:)]){
                [self.delegate tempo:[[[NSNumber alloc]initWithUnsignedInt:*(unsigned int *) data]integerValue]];
            }
            break;
        case BSMD_CALLBACK_TYPE_TIME_SIGNATURE:
#if __LP64__
            NSLog (@"set time signature = %u", *(unsigned int *) data);
#else
            NSLog (@"set time signature = %lu", *(unsigned long *) data);
#endif
            unsigned char beatsPerMeasure = (*(unsigned int *) data >> 24) & 0x000000FF;
            unsigned char whatNoteIsTheBeat = (*(unsigned int *) data >> 16) & 0x000000FF;
            unsigned char clockTicksPerQuarterNote  = (*(unsigned int *) data >> 8) & 0x000000FF;
            unsigned char ndNotesPerQuarterNote = (*(unsigned int *) data >> 0) & 0x000000FF;
            
            self.beatsPerMeasure = beatsPerMeasure;
            self.whatNoteIsTheBeat = whatNoteIsTheBeat;
            self.clockTicksPerQuarterNote = clockTicksPerQuarterNote;
            self.ndNotesPerQuarterNote = ndNotesPerQuarterNote;
            if(self.ndNotesPerQuarterNoteDefault == -1){
                self.ndNotesPerQuarterNoteDefault = ndNotesPerQuarterNote;
            }
            
            NSLog (@"TIME_SIGNATURE beatsPerMeasure %d whatNoteIsTheBeat %d clockTicksParQuarterNote %d ndNotesPerQuarterNote %d",beatsPerMeasure, whatNoteIsTheBeat, clockTicksPerQuarterNote,ndNotesPerQuarterNote);
            
            if([self.delegate respondsToSelector:@selector(signature:)]){
                [self.delegate signature:[[[NSNumber alloc]initWithUnsignedInt:*(unsigned int *) data]integerValue]];
            }
            break;
        case BSMD_CALLBACK_TYPE_CHANNEL_MESSAGE:
            
        {
            //        unsigned char midiPort = (*(unsigned long *) data >> 24) & 0x000000FF;
            //        unsigned char status = (*(unsigned long *) data >> 16) & 0x000000FF;
            //        unsigned char channel = status & 0xF;
            //        unsigned char data1 = (*(unsigned long *) data >> 8) & 0x000000FF;
            //        unsigned char data2 = (*(unsigned long *) data >> 0) & 0x000000FF;
            ////        NSLog (@"channel message %02X %02X %02X %02X",midiPort, status, data1, data2);
            //          printf("channel message midiPort "BYTE_TO_BINARY_PATTERN" \n", BYTE_TO_BINARY(midiPort));
            //          printf("channel message status "BYTE_TO_BINARY_PATTERN" \n", BYTE_TO_BINARY(status));
            //          printf("channel message channel "BYTE_TO_BINARY_PATTERN" \n", BYTE_TO_BINARY(channel));
            //          printf("channel message data1 "BYTE_TO_BINARY_PATTERN" \n", BYTE_TO_BINARY(data1));
            //          printf("channel message data2 "BYTE_TO_BINARY_PATTERN" \n", BYTE_TO_BINARY(data2));
            //          printf("\n");
            
//            const UInt8 note      = 1;
//            const UInt8 noteOn[]  = { 0x90, note, 127 };
//            const UInt8 noteOff[] = { 0x80, note, 0   };
            
//            UInt8 newData = *(unsigned long *) data & 0x000000FF;
//
//            [self.midi sendBytes:data size:sizeof(data)];
            
            
            if([self.delegate respondsToSelector:@selector(channelMessage)]){
                [self.delegate channelMessage];
            }
        }
           
            break;
        case BSMD_CALLBACK_TYPE_SYSTEM_EXCLUSIVE_MESSAGE:
            NSLog(@"BSMD_CALLBACK_TYPE_SYSTEM_EXCLUSIVE_MESSAGE");
            break;
      
        default:
            break;
    }
}

-(void)calculateCurrentTime{
    CGFloat secondsPerQuarterNote = self.microsecondsPerQuarterNote / 1000000.0f;
    CGFloat secondsPerTick = secondsPerQuarterNote / self.clockTicksPerQuarterNote;
    self.currentTime+=secondsPerTick;
    
    printf("currentTime %f \n",self.currentTime);
}

@end
