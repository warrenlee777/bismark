//
//  TableViewController.h
//  sample
//
//  Created by warren on 2017/12/29.
//  Copyright © 2017年 bismark. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol TableViewControllerDele<NSObject>

-(void)selectSouece:(NSInteger)index;
-(void)selectDestion:(NSInteger)index;

@end

@interface TableViewController : UITableViewController

@property (nonatomic,weak) id<TableViewControllerDele> dele;

@end
