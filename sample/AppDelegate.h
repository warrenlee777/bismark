//
//  AppDelegate.h
//  sample
//
//  Created by hideo on 10/8/13.
//  Copyright (c) 2013 bismark. All rights reserved.
//

#import <UIKit/UIKit.h>

#include "bsmp.h"

@protocol AppDelegateDelegate<NSObject>

-(void)opened;
-(void)closeed;
-(void)started;
-(void)stopped;
-(void)seeked;
-(void)channelMessage;

@end

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@property (weak) id<AppDelegateDelegate> delegate;


@property BSMP_FUNC *api;
@property BSMP_HANDLE handle;
@property NSInteger clocks;

@end
