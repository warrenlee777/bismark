//
//  ViewController.m
//  sample
//
//  Created by hideo on 10/8/13.
//  Copyright (c) 2013 bismark. All rights reserved.
//

#import "AppDelegate.h"
#import "ViewController.h"

@interface ViewController ()<AppDelegateDelegate,UITableViewDataSource,UITableViewDelegate>
{
  NSTimer *timer;
	unsigned short division;
}

@property (weak, nonatomic) IBOutlet UISlider *seekSlider;
@property (weak, nonatomic) IBOutlet UISwitch *reverbSwitch;
@property (weak, nonatomic) IBOutlet UILabel *reverbs;
@property (weak, nonatomic) IBOutlet UILabel *reverbAvilable;
@property (weak, nonatomic) IBOutlet UISwitch *chorusSwitch;
@property (weak, nonatomic) IBOutlet UILabel *choru;
@property (weak, nonatomic) IBOutlet UILabel *chorusAviable;
@property (weak, nonatomic) IBOutlet UILabel *keyLabel;
@property (weak, nonatomic) IBOutlet UILabel *speedLabel;
@property (weak, nonatomic) IBOutlet UILabel *volumeLabel;
@property (weak, nonatomic) IBOutlet UILabel *tuneLabel;
@property (weak, nonatomic) IBOutlet UILabel *guideLabel;
@property (weak, nonatomic) IBOutlet UILabel *delayLabel;
@property (weak, nonatomic) IBOutlet UILabel *delayAviable;
@property (weak, nonatomic) IBOutlet UILabel *sampleRateLabel;
@property (weak, nonatomic) IBOutlet UILabel *channelsLabel;
@property (weak, nonatomic) IBOutlet UILabel *polyLabel;
@property (weak, nonatomic) IBOutlet UIScrollView *channelScrollView;
@property (weak, nonatomic) IBOutlet UITableView *midiTableView;
@property (nonatomic) NSMutableArray *trackVolumeArray;
@property (nonatomic) NSMutableArray *trackMuteArray;
@property (nonatomic) NSMutableArray *trackSoloArray;
@property (nonatomic) NSMutableArray *trackInstrumentArray;
@property (nonatomic) NSMutableArray *instrumentArray;
@property (weak, nonatomic) IBOutlet UIStepper *keyStepper;
@property (weak, nonatomic) IBOutlet UIStepper *speedSteeper;
@property (weak, nonatomic) IBOutlet UIStepper *volumeSteeper;
@property (weak, nonatomic) IBOutlet UIStepper *tuneSteeper;
@property (weak, nonatomic) IBOutlet UIStepper *guideSteeper;
@property (weak, nonatomic) IBOutlet UIStepper *sampleSteeper;
@property (weak, nonatomic) IBOutlet UIStepper *delaySteeper;
@property (weak, nonatomic) IBOutlet UIStepper *polySteeper;
@property (weak, nonatomic) IBOutlet UILabel *soundNumber;
@property (weak, nonatomic) IBOutlet UILabel *soundMode;
@property (weak, nonatomic) IBOutlet UILabel *instrumentFix;
@property (nonatomic) NSArray *midiArray;
@property (nonatomic) NSInteger currentMidiIndex;


@end

@implementation ViewController


- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view, typically from a nib.
    
    self.trackVolumeArray = [[NSMutableArray alloc] init];
    self.trackMuteArray = [[NSMutableArray alloc] init];
    self.trackSoloArray = [[NSMutableArray alloc] init];
    self.trackInstrumentArray = [[NSMutableArray alloc] init];
    self.midiArray = [[NSBundle mainBundle] URLsForResourcesWithExtension:@"midi" subdirectory:nil];
    
    UILabel *descriptionLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, self.channelScrollView.frame.size.width, 30)];
    [self.channelScrollView addSubview:descriptionLabel];
    descriptionLabel.text = @"instrument                           solo      mute     volume";
    self.channelScrollView.contentSize = CGSizeMake(self.channelScrollView.frame.size.width, descriptionLabel.frame.size.height);
    for(int i=0;i<16;i++){
        [self addTrack];
    }
    
    AppDelegate *appDelegate = (AppDelegate *) [[UIApplication sharedApplication]delegate];
    appDelegate.delegate = self;
    
    self.midiTableView.dataSource = self;
    self.midiTableView.delegate = self;
    
    self.currentMidiIndex = 0;
}

- (void)viewWillAppear:(BOOL)animated
{
  [super viewWillAppear:animated];

	
    [self loadMidi:[[((NSString*)[self.midiArray objectAtIndex:self.currentMidiIndex])lastPathComponent]stringByDeletingPathExtension]];
}

-(void)loadMidi:(NSString*)midiFilePath{
    AppDelegate *appDelegate = (AppDelegate *) [[UIApplication sharedApplication]delegate];
    BSMP_ERR err = BSMP_OK;
    
    if (err == BSMP_OK) {
        // revweb on
        int value = (int) self.reverbSwitch.isOn;
        err = appDelegate.api->ctrl (appDelegate.handle, BSMP_CTRL_SET_REVERB, &value, sizeof (value));
    }
    
    if (err == BSMP_OK) {
        // chorus on
        int value = (int) self.chorusSwitch.isOn;
        err = appDelegate.api->ctrl (appDelegate.handle, BSMP_CTRL_SET_CHORUS, &value, sizeof (value));
    }
    
    if (err == BSMP_OK) {
        // set midi contents
        NSString *path = [[NSBundle mainBundle] pathForResource:midiFilePath ofType:@"midi"];
        const char *lib = [path cStringUsingEncoding:NSShiftJISStringEncoding];
        err = appDelegate.api->setFile (appDelegate.handle, lib);
        
        
    }
    
    if (err == BSMP_OK) {
        [self initSeek];
        
        self.instrumentArray = [[NSMutableArray alloc] init];
        for(int i=0;i<16;i++){
            [self.instrumentArray addObject:[self getBSMP_CTRL_GET_INSTRUMENT_NAME:i]];
        }
        
       
        
    }
    
    if (err == BSMP_OK) {
        
        // open wave output device
        int numDrivers = appDelegate.api->getNumDrivers(appDelegate.handle);
        NSLog(@"numDrivers %d",numDrivers);
        LPCTSTR driverName = appDelegate.api->getDriverName(appDelegate.handle,0);
        NSLog(@"driverName %s",driverName);
        int numDevices = appDelegate.api->getNumDevices(appDelegate.handle,driverName);
        NSLog(@"numDevices %d",numDevices);
        LPCTSTR deviceName = appDelegate.api->getDeviceName(appDelegate.handle,driverName,0);
        NSLog(@"deviceName %s",deviceName);
        err = appDelegate.api->open (appDelegate.handle, NULL, NULL);
        
        
        
        [self getVolumeValue];
        [self getSpeedValue];
        [self getKeyValue];
        [self getTuneValue];
        [self getGuideValue];
        [self getBSMP_CTRL_GET_REVERBValue];
        [self getBSMP_CTRL_GET_REVERB_AVAILABLEValue];
        [self getBSMP_CTRL_GET_CHORUSValue];
        [self getBSMP_CTRL_GET_CHORUS_AVAILABLEValue];
        [self setBSMP_CTRL_GET_DELAY_AVAILABLEValue];
        [self getDelayValue];
        [self getBSMP_CTRL_GET_SOUND_LIBRARY_NUMValue];
        [self getBSMP_CTRL_GET_SOUND_LIBRARY_SELValue];
        [self getBSMP_CTRL_GET_NO_INSTRUMENT_FIXValue];
        [self getSampleRateValue];
        [self getChannelsValue];
        [self getPolyValue];
        
        
    }
}

- (void)viewWillDisappear:(BOOL)animated
{
  [super viewWillDisappear:animated];

	AppDelegate *appDelegate = (AppDelegate *) [[UIApplication sharedApplication]delegate];
  BSMP_ERR err = BSMP_OK;

	if (err == BSMP_OK) {
		// close wave output device
		err = appDelegate.api->close (appDelegate.handle);
	}
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

- (IBAction)start:(id)sender
{
	AppDelegate *appDelegate = (AppDelegate *) [[UIApplication sharedApplication]delegate];
	if (appDelegate.api->isPlaying (appDelegate.handle) == 0) {
		appDelegate.api->start (appDelegate.handle);
    timer = [NSTimer scheduledTimerWithTimeInterval:1.0f target:self selector:@selector(updateSlider:) userInfo:nil repeats:YES];
	}
    [self trackVolumeSlider:nil];
}

- (IBAction)stop:(id)sender
{
    [self stop];
}

-(void)stop{
    AppDelegate *appDelegate = (AppDelegate *) [[UIApplication sharedApplication]delegate];
    if (appDelegate.api->isPlaying (appDelegate.handle) == 1) {
        appDelegate.api->stop (appDelegate.handle);
        [timer invalidate];
    }
}

- (IBAction)seek:(UISlider *)sender
{
	AppDelegate *appDelegate = (AppDelegate *) [[UIApplication sharedApplication]delegate];
	appDelegate.clocks = 0;
	unsigned long tick = sender.value * division / 24;
	NSLog (@"seek %lu tick = %.0f MIDI clocks", tick, sender.value);
	appDelegate.api->seek (appDelegate.handle, tick);
}

- (void)updateSlider:(NSTimer *)timer
{
    AppDelegate *appDelegate = (AppDelegate *) [[UIApplication sharedApplication]delegate];
    if (self.seekSlider.touchInside == NO) {
        [self.seekSlider setValue:appDelegate.clocks];
    }
}

-(void)initSeek{
    AppDelegate *appDelegate = (AppDelegate *) [[UIApplication sharedApplication]delegate];
    
    unsigned long totaltick;
    appDelegate.api->getFileInfo (appDelegate.handle, NULL, &division, &totaltick, NULL);
    NSInteger clocks = totaltick * 24 / division;
    NSLog (@"total %lu tick = %ld MIDI clocks", totaltick, (long)clocks);
    [self.seekSlider setMaximumValue:clocks];
    appDelegate.clocks = 0;
    [self.seekSlider setValue:appDelegate.clocks];
    unsigned long tick = 0.0 * division / 24;
    NSLog (@"seek %lu tick = %.0f MIDI clocks", tick, 0.0);
    appDelegate.api->seek (appDelegate.handle, tick);
}

- (IBAction)keyControl:(UIStepper *)sender
{
  int value = sender.value;
	AppDelegate *appDelegate = (AppDelegate *) [[UIApplication sharedApplication]delegate];
  appDelegate.api->ctrl (appDelegate.handle, BSMP_CTRL_SET_MASTER_KEY, &value, sizeof (value));
    
    [self getKeyValue];
}

-(void)getKeyValue{
    int result;
    AppDelegate *appDelegate = (AppDelegate *) [[UIApplication sharedApplication]delegate];
    appDelegate.api->ctrl (appDelegate.handle, BSMP_CTRL_GET_MASTER_KEY, &result, sizeof (result));
    
    self.keyLabel.text = [NSString stringWithFormat:@"MASTER_KEY %D", result];
    
    self.keyStepper.value = result;
}

- (IBAction)speedControl:(UIStepper *)sender {
  int value = sender.value;
	AppDelegate *appDelegate = (AppDelegate *) [[UIApplication sharedApplication]delegate];
  appDelegate.api->ctrl (appDelegate.handle, BSMP_CTRL_SET_SPEED, &value, sizeof (value));
    
    [self getSpeedValue];
}

-(void)getSpeedValue{
    int result;
    AppDelegate *appDelegate = (AppDelegate *) [[UIApplication sharedApplication]delegate];
    appDelegate.api->ctrl (appDelegate.handle, BSMP_CTRL_GET_SPEED, &result, sizeof (result));
    
    self.speedLabel.text = [NSString stringWithFormat:@"SPEED %d",result];
    
    self.speedSteeper.value = result;
}

- (IBAction)reverb:(UISwitch *)sender
{
  int value = sender.isOn;
	AppDelegate *appDelegate = (AppDelegate *) [[UIApplication sharedApplication]delegate];
  appDelegate.api->ctrl (appDelegate.handle, BSMP_CTRL_SET_REVERB, &value, sizeof (value));
    
    [self getBSMP_CTRL_GET_REVERBValue];
}

-(void)getBSMP_CTRL_GET_REVERBValue{
    int result;
    AppDelegate *appDelegate = (AppDelegate *) [[UIApplication sharedApplication]delegate];
    appDelegate.api->ctrl (appDelegate.handle, BSMP_CTRL_GET_REVERB, &result, sizeof (result));
    
    self.reverbs.text = [NSString stringWithFormat:@"REVERB %d",result];
}

-(void)getBSMP_CTRL_GET_REVERB_AVAILABLEValue{
    int result;
    AppDelegate *appDelegate = (AppDelegate *) [[UIApplication sharedApplication]delegate];
    appDelegate.api->ctrl (appDelegate.handle, BSMP_CTRL_GET_REVERB_AVAILABLE, &result, sizeof (result));
    
    self.reverbAvilable.text = [NSString stringWithFormat:@"AVAILABLE %d",result];
}

- (IBAction)chorus:(UISwitch *)sender
{
  int value = sender.isOn;
	AppDelegate *appDelegate = (AppDelegate *) [[UIApplication sharedApplication]delegate];
  appDelegate.api->ctrl (appDelegate.handle, BSMP_CTRL_SET_CHORUS, &value, sizeof (value));
    
    [self getBSMP_CTRL_GET_CHORUSValue];
}

-(void)getBSMP_CTRL_GET_CHORUSValue{
    int result;
    AppDelegate *appDelegate = (AppDelegate *) [[UIApplication sharedApplication]delegate];
    appDelegate.api->ctrl (appDelegate.handle, BSMP_CTRL_GET_CHORUS, &result, sizeof (result));
    
    self.choru.text = [NSString stringWithFormat:@"CHORUS %d",result];
}

-(void)getBSMP_CTRL_GET_CHORUS_AVAILABLEValue{
    int result;
    AppDelegate *appDelegate = (AppDelegate *) [[UIApplication sharedApplication]delegate];
    appDelegate.api->ctrl (appDelegate.handle, BSMP_CTRL_GET_CHORUS_AVAILABLE, &result, sizeof (result));
    
    self.chorusAviable.text = [NSString stringWithFormat:@"AVAILABLE %d",result];
}

- (IBAction)volume:(UIStepper *)sender {
    int value = sender.value;
    AppDelegate *appDelegate = (AppDelegate *) [[UIApplication sharedApplication]delegate];
    BSMP_ERR err = BSMP_OK;
    err = appDelegate.api->ctrl (appDelegate.handle, BSMP_CTRL_SET_MASTER_VOLUME, &value, sizeof (value));
    if (err == BSMP_OK) {
        
        
    }else{
        NSLog(@"%d",err);
    }
    [self getVolumeValue];
}

-(void)getVolumeValue{
    int result;
    AppDelegate *appDelegate = (AppDelegate *) [[UIApplication sharedApplication]delegate];
    appDelegate.api->ctrl (appDelegate.handle, BSMP_CTRL_GET_MASTER_VOLUME, &result, sizeof (result));
    
    self.volumeLabel.text = [NSString stringWithFormat:@"MASTER_VOLUME %d",result];
    self.volumeSteeper.value = result;
}

- (IBAction)tune:(UIStepper*)sender {
    int value = sender.value;
    AppDelegate *appDelegate = (AppDelegate *) [[UIApplication sharedApplication]delegate];
    appDelegate.api->ctrl (appDelegate.handle, BSMP_CTRL_SET_MASTER_TUNE, &value, sizeof (value));
    
    [self getTuneValue];
}

-(void)getTuneValue{
    int result;
    AppDelegate *appDelegate = (AppDelegate *) [[UIApplication sharedApplication]delegate];
    appDelegate.api->ctrl (appDelegate.handle, BSMP_CTRL_GET_MASTER_TUNE, &result, sizeof (result));
    
    self.tuneLabel.text = [NSString stringWithFormat:@"MASTER_TUNE %d",result];
    self.tuneSteeper.value = result;
}

- (IBAction)guide:(UIStepper*)sender {
    int value = sender.value;
    AppDelegate *appDelegate = (AppDelegate *) [[UIApplication sharedApplication]delegate];
    BSMP_ERR err = BSMP_OK;
    err = appDelegate.api->ctrl (appDelegate.handle, BSMP_CTRL_SET_GUIDE, &value, sizeof (value));
    
    if (err == BSMP_OK) {
       
       
    }else{
        NSLog(@"%d",err);
    }
    
    [self getGuideValue];
}

-(void)getGuideValue{
    int result;
    AppDelegate *appDelegate = (AppDelegate *) [[UIApplication sharedApplication]delegate];
    appDelegate.api->ctrl (appDelegate.handle, BSMP_CTRL_GET_GUIDE, &result, sizeof (result));
    
    self.guideLabel.text = [NSString stringWithFormat:@"GUIDE %d",result];
    self.guideSteeper.value = result;
}

- (IBAction)delay:(UIStepper*)sender {
    
    int value = sender.value;
    AppDelegate *appDelegate = (AppDelegate *) [[UIApplication sharedApplication]delegate];
    appDelegate.api->ctrl (appDelegate.handle, BSMP_CTRL_SET_DELAY, &value, sizeof (value));
    
    [self getDelayValue];
    
}

-(void)getDelayValue{
    int result;
    AppDelegate *appDelegate = (AppDelegate *) [[UIApplication sharedApplication]delegate];
    appDelegate.api->ctrl (appDelegate.handle, BSMP_CTRL_GET_DELAY, &result, sizeof (result));
    
    self.delayLabel.text = [NSString stringWithFormat:@"DELAY %d",result];
    self.delaySteeper.value = result;
}

-(void)setBSMP_CTRL_GET_DELAY_AVAILABLEValue{
    int result;
    AppDelegate *appDelegate = (AppDelegate *) [[UIApplication sharedApplication]delegate];
    appDelegate.api->ctrl (appDelegate.handle, BSMP_CTRL_GET_DELAY_AVAILABLE, &result, sizeof (result));
    
    self.delayAviable.text = [NSString stringWithFormat:@"AVAILABLE %d",result];
}

- (IBAction)sampleRate:(UIStepper*)sender {
    int value = sender.value;
    AppDelegate *appDelegate = (AppDelegate *) [[UIApplication sharedApplication]delegate];
    appDelegate.api->ctrl (appDelegate.handle, BSMP_CTRL_SET_SAMPLE_RATE, &value, sizeof (value));
    
    [self getSampleRateValue];
}

-(void)getSampleRateValue{
    int result;
    AppDelegate *appDelegate = (AppDelegate *) [[UIApplication sharedApplication]delegate];
    appDelegate.api->ctrl (appDelegate.handle, BSMP_CTRL_GET_SAMPLE_RATE, &result, sizeof (result));
    
    self.sampleRateLabel.text = [NSString stringWithFormat:@"SAMPLE_RATE %d",result];
    self.sampleSteeper.value = result;
}

-(void)getChannelsValue{
    int result;
    AppDelegate *appDelegate = (AppDelegate *) [[UIApplication sharedApplication]delegate];
    appDelegate.api->ctrl (appDelegate.handle, BSMP_CTRL_GET_CHANNELS, &result, sizeof (result));
    
    self.channelsLabel.text = [NSString stringWithFormat:@"CHANNELS %d",result];
}

- (IBAction)poly:(UIStepper*)sender {
    int value = sender.value;
    AppDelegate *appDelegate = (AppDelegate *) [[UIApplication sharedApplication]delegate];
    appDelegate.api->ctrl (appDelegate.handle, BSMP_CTRL_SET_POLY, &value, sizeof (value));
    
    [self getPolyValue];
}

-(void)getPolyValue{
    int result;
    AppDelegate *appDelegate = (AppDelegate *) [[UIApplication sharedApplication]delegate];
    appDelegate.api->ctrl (appDelegate.handle, BSMP_CTRL_GET_POLY, &result, sizeof (result));
    
    self.polyLabel.text = [NSString stringWithFormat:@"POLY %d",result];
}

- (IBAction)INSTRUMENT_FIX:(UISwitch*)sender {
    int value = sender.isOn;
    AppDelegate *appDelegate = (AppDelegate *) [[UIApplication sharedApplication]delegate];
    appDelegate.api->ctrl (appDelegate.handle, BSMP_CTRL_SET_NO_INSTRUMENT_FIX, &value, sizeof (value));
}

-(void)getBSMP_CTRL_GET_NO_INSTRUMENT_FIXValue{
    int result;
    AppDelegate *appDelegate = (AppDelegate *) [[UIApplication sharedApplication]delegate];
    appDelegate.api->ctrl (appDelegate.handle, BSMP_CTRL_GET_NO_INSTRUMENT_FIX, &result, sizeof (result));
    
    self.instrumentFix.text = [NSString stringWithFormat:@"INSTRUMENT_FIX %d",result];
}

-(NSString*)getBSMP_CTRL_GET_INSTRUMENT_NAME:(int)track{
    char result[100];
    AppDelegate *appDelegate = (AppDelegate *) [[UIApplication sharedApplication]delegate];
    appDelegate.api->ctrl (appDelegate.handle, BSMP_CTRL_GET_INSTRUMENT_NAME+track , result, sizeof (result));
//    NSLog(@"instrument name %@",[NSString stringWithFormat:@"%s", result]);
    return [NSString stringWithFormat:@"%s", result];
}

-(void)getBSMP_CTRL_GET_SOUND_LIBRARY_NUMValue{
    int result;
    AppDelegate *appDelegate = (AppDelegate *) [[UIApplication sharedApplication]delegate];
    appDelegate.api->ctrl (appDelegate.handle, BSMP_CTRL_GET_SOUND_LIBRARY_NUM, &result, sizeof (result));
    
    self.soundNumber.text = [NSString stringWithFormat:@"SOUND_LIBRARY %d",result];
    
}

-(void)getBSMP_CTRL_GET_SOUND_LIBRARY_SELValue{
    int result;
    AppDelegate *appDelegate = (AppDelegate *) [[UIApplication sharedApplication]delegate];
    appDelegate.api->ctrl (appDelegate.handle, BSMP_CTRL_GET_SOUND_LIBRARY_SEL, &result, sizeof (result));
    
    self.soundMode.text = [NSString stringWithFormat:@"SOUND_MODE %d",result];
}

-(void)addTrack{
    UISlider *slider = [[UISlider alloc] initWithFrame:CGRectMake(320, self.channelScrollView.contentSize.height, self.channelScrollView.frame.size.width-320, 30)];
    [slider addTarget:self action:@selector(trackVolumeSlider:) forControlEvents:UIControlEventValueChanged];
    [self.channelScrollView addSubview:slider];
    UISwitch *mute = [[UISwitch alloc] initWithFrame:CGRectMake(260, slider.frame.origin.y, 60, 30)];
    [mute addTarget:self action:@selector(muteSwitch:) forControlEvents:UIControlEventValueChanged];
    [self.channelScrollView addSubview:mute];
    UISwitch *solo = [[UISwitch alloc] initWithFrame:CGRectMake(200, slider.frame.origin.y, 60, 30)];
    [solo addTarget:self action:@selector(soloSwitch:) forControlEvents:UIControlEventValueChanged];
    [self.channelScrollView addSubview:solo];
    UILabel *instrument = [[UILabel alloc] initWithFrame:CGRectMake(0, slider.frame.origin.y, 200, 30)];
    [self.channelScrollView addSubview:instrument];
    [self.trackVolumeArray addObject:slider];
    [self.trackMuteArray addObject:mute];
    [self.trackSoloArray addObject:solo];
    [self.trackInstrumentArray addObject:instrument];
    self.channelScrollView.contentSize = CGSizeMake(self.channelScrollView.frame.size.width, slider.frame.origin.y+slider.frame.size.height);
}

-(void)removeAllTrack{
    for(UIView *view in self.trackVolumeArray){
        [view removeFromSuperview];
    }
    [self.trackVolumeArray removeAllObjects];
    for(UIView *view in self.trackMuteArray){
        [view removeFromSuperview];
    }
    [self.trackMuteArray removeAllObjects];
    for(UIView *view in self.trackSoloArray){
        [view removeFromSuperview];
    }
    [self.trackSoloArray removeAllObjects];
    for(UIView *view in self.trackInstrumentArray){
        [view removeFromSuperview];
    }
    [self.trackInstrumentArray removeAllObjects];
}

-(void)trackVolumeSlider:(UISlider*)sender{
    for(int i=0;i<[self.trackVolumeArray count]*2;i++){
        int value = 9;
        AppDelegate *appDelegate = (AppDelegate *) [[UIApplication sharedApplication]delegate];
        BSMP_ERR err = BSMP_OK;
        err = appDelegate.api->ctrl (appDelegate.handle, BSMP_CTRL_SET_GUIDE_MAIN_CH, &value, sizeof (value));
        
        if (err == BSMP_OK) {
            NSLog(@"BSMP_OK %d",i);
        }else{
            NSLog(@"BSMP_ERR %d",err);
        }
        int volume = -2;
        err = appDelegate.api->ctrl (appDelegate.handle, BSMP_CTRL_SET_GUIDE, &volume, sizeof (volume));
        
        if (err == BSMP_OK) {
            
            
        }else{
            NSLog(@"%d",err);
        }
        
        err = appDelegate.api->ctrl (appDelegate.handle, BSMP_CTRL_GET_GUIDE_MAIN_CH, &value, sizeof (value));
//        err = appDelegate.api->ctrl (appDelegate.handle, BSMP_CTRL_SET_GUIDE_SUB_CH, &value, sizeof (value));
//
//        if (err == BSMP_OK) {
//            NSLog(@"BSMP_OK %d",i);
//        }else{
//            NSLog(@"BSMP_ERR %d",err);
//        }
    }
}

-(void)muteSwitch:(UISwitch*)sender{
    int track = 0;
    int value;
    if(sender.isOn){
        value = 1;
    }else{
        value = 0;
    }
    for(int i=0;i<[self.trackMuteArray count];i++){
        UISwitch *uiSwitch = [self.trackMuteArray objectAtIndex:i];
        if(uiSwitch == sender){
            track = i;
        }
    }
    
    
    AppDelegate *appDelegate = (AppDelegate *) [[UIApplication sharedApplication]delegate];
    appDelegate.api->ctrl (appDelegate.handle, BSMP_CTRL_SET_MUTE+track, &value, sizeof (value));
}

-(void)soloSwitch:(UISwitch*)sender{
    int track = 0;
    int value;
    if(sender.isOn){
        value = 1;
    }else{
        value = 0;
    }
    for(int i=0;i<[self.trackSoloArray count];i++){
        UISwitch *uiSwitch = [self.trackSoloArray objectAtIndex:i];
        if(uiSwitch == sender){
            track = i;
        }else{
            if(sender.isOn){
                [uiSwitch setOn:NO];
            }
        }
        
    }
    
    
    AppDelegate *appDelegate = (AppDelegate *) [[UIApplication sharedApplication]delegate];
    appDelegate.api->ctrl (appDelegate.handle, BSMP_CTRL_SET_SOLO+track, &value, sizeof (value));
}

-(void)updateTrackInstrument:(int)track withName:(NSString*)name{
    UILabel *label = [self.trackInstrumentArray objectAtIndex:track];
    label.text = name;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return [self.midiArray count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    UITableViewCell *cell = [[UITableViewCell alloc] init];
    cell.textLabel.text = [[[self.midiArray objectAtIndex:indexPath.row] lastPathComponent] stringByDeletingPathExtension];
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    if(self.currentMidiIndex != indexPath.row){
        self.currentMidiIndex = indexPath.row;
        [self stop];
        [self loadMidi:[[((NSString*)[self.midiArray objectAtIndex:self.currentMidiIndex])lastPathComponent]stringByDeletingPathExtension]];
    }
    
}

-(void)opened{
    
//    for(int i=0;i<16;i++){
//        [self updateTrackInstrument:i];
//    }
}

-(void)closeed{
    
//    for(int i=0;i<16;i++){
//        [self updateTrackInstrument:i];
//    }
}

-(void)started{
    
//    for(int i=0;i<16;i++){
//        [self updateTrackInstrument:i];
//    }
}

-(void)stopped{
    
//    for(int i=0;i<16;i++){
//        [self updateTrackInstrument:i];
//    }
}

-(void)seeked{
    
    for(int i=0;i<16;i++){
        dispatch_async(dispatch_get_main_queue(), ^(void){
            NSString *name = [self getBSMP_CTRL_GET_INSTRUMENT_NAME:i];
            [self updateTrackInstrument:i withName:name];
        });
       
    }
}

-(void)channelMessage{
    for(int i=0;i<16;i++){
        dispatch_async(dispatch_get_main_queue(), ^(void){
            NSString *oldName = [self.instrumentArray objectAtIndex:i];
            NSString *name = [self getBSMP_CTRL_GET_INSTRUMENT_NAME:i];
            if(![oldName isEqualToString:name]){
                [self updateTrackInstrument:i withName:name];
            }
        });
        
        
    }
}

@end
